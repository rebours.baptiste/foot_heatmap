# Imports
import gpxpy
import gmplot



# Global function
def gpx_to_heatmap_gglmps(gpx_file, api_key, html_output):

    # Parsing GPX file
    gpx = open(gpx_file, 'r')
    gpx = gpxpy.parse(gpx)

    # Getting latitudes and longitudes
    lat = []
    lon = []
    for track in gpx.tracks:
        for segment in track.segments:
            for point in segment.points:
                lat.append(point.latitude)
                lon.append(point.longitude)

    # Draw on Google Maps
    gmap = gmplot.GoogleMapPlotter(lat[0], lon[0], 18, map_type='hybrid', apikey=api_key)
    gmap.heatmap(lat, lon)
    gmap.draw(html_output)


