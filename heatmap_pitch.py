# Imports
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Arc
import seaborn as sns
import gpxpy



### Lat long to X Y
def latlongToXY(lat, long, top_left_corner, bottom_right_corner):

    # Computing X
    x = 6.371*long*np.cos((top_left_corner['lat'] + bottom_right_corner['lat'])/2)
    # Computing Y
    y = 6.371*lat

    return {'x':x, 'y':y}




# Lat long to screen X Y
def latlongToScreenXY(lat, long, top_left_corner, bottom_right_corner, tl_corner_globalXY, br_corner_globalXY):

    # Calculate global X and Y for projection point
    pos = latlongToXY(lat, long, top_left_corner, bottom_right_corner)
    # Calculate the percentage of Global X position in relation to total global width
    pos['x'] = ((pos['x']-tl_corner_globalXY['x'])/(br_corner_globalXY['x'] - tl_corner_globalXY['x']))
    # Calculate the percentage of Global Y position in relation to total global height
    pos['y'] = ((pos['y']-tl_corner_globalXY['y'])/(br_corner_globalXY['y'] - tl_corner_globalXY['y']))
    # Returns the screen position based on reference points
    return {
        'x': top_left_corner['x'] + (bottom_right_corner['x'] - top_left_corner['x'])*pos['x'], 
        'y': top_left_corner['y'] + (bottom_right_corner['y'] - top_left_corner['y'])*pos['y']
    }



# Draw football pitch
def draw_football_pitch(all_X, all_Y, png_output):

    #Create figure
    fig=plt.figure()
    fig.set_size_inches(7, 5)
    ax=fig.add_subplot(1,1,1)

    #Pitch Outline & Centre Line
    plt.plot([0,0],[0,90], color="black")
    plt.plot([0,130],[90,90], color="black")
    plt.plot([130,130],[90,0], color="black")
    plt.plot([130,0],[0,0], color="black")
    plt.plot([65,65],[0,90], color="black")

    #Left Penalty Area
    plt.plot([16.5,16.5],[65,25],color="black")
    plt.plot([0,16.5],[65,65],color="black")
    plt.plot([16.5,0],[25,25],color="black")

    #Right Penalty Area
    plt.plot([130,113.5],[65,65],color="black")
    plt.plot([113.5,113.5],[65,25],color="black")
    plt.plot([113.5,130],[25,25],color="black")

    #Left 6-yard Box
    plt.plot([0,5.5],[54,54],color="black")
    plt.plot([5.5,5.5],[54,36],color="black")
    plt.plot([5.5,0.5],[36,36],color="black")

    #Right 6-yard Box
    plt.plot([130,124.5],[54,54],color="black")
    plt.plot([124.5,124.5],[54,36],color="black")
    plt.plot([124.5,130],[36,36],color="black")

    #Prepare Circles
    centreCircle = plt.Circle((65,45),9.15,color="black",fill=False)
    centreSpot = plt.Circle((65,45),0.8,color="black")
    leftPenSpot = plt.Circle((11,45),0.8,color="black")
    rightPenSpot = plt.Circle((119,45),0.8,color="black")

    #Draw Circles
    ax.add_patch(centreCircle)
    ax.add_patch(centreSpot)
    ax.add_patch(leftPenSpot)
    ax.add_patch(rightPenSpot)

    #Prepare Arcs
    leftArc = Arc((11,45),height=18.3,width=18.3,angle=0,theta1=310,theta2=50,color="black")
    rightArc = Arc((119,45),height=18.3,width=18.3,angle=0,theta1=130,theta2=230,color="black")

    #Draw Arcs
    ax.add_patch(leftArc)
    ax.add_patch(rightArc)

    #Tidy Axes
    plt.axis('off')


    sns.kdeplot(all_X, all_Y, shade=True, n_levels=50)
    plt.ylim(0, 90)
    plt.xlim(0, 130)

    plt.savefig(png_output)




# Global function
def gpx_to_heatmap_pitch(gpx_file, png_output, top_left_corner, bottom_right_corner):

    # Parsing GPX file
    gpx = open(gpx_file, 'r')
    gpx = gpxpy.parse(gpx)

    # Getting global X Y of top-left and bottom-right corners
    tl_corner_globalXY = latlongToXY(top_left_corner['lat'],top_left_corner['long'],top_left_corner,bottom_right_corner)
    br_corner_globalXY = latlongToXY(bottom_right_corner['lat'],bottom_right_corner['long'],top_left_corner,bottom_right_corner)

    # Get all X and Y
    all_X = []
    all_Y = []
    for track in gpx.tracks:
        for segment in track.segments:
            for point in segment.points:
                all_X.append(latlongToScreenXY(point.latitude, point.longitude, top_left_corner, bottom_right_corner, tl_corner_globalXY, br_corner_globalXY)['x'])
                all_Y.append(latlongToScreenXY(point.latitude, point.longitude, top_left_corner, bottom_right_corner, tl_corner_globalXY, br_corner_globalXY)['y'])
    
    # Draw football pitch
    draw_football_pitch(all_X, all_Y, png_output)


