# Imports
from heatmap_maps import gpx_to_heatmap_gglmps
from heatmap_pitch import gpx_to_heatmap_pitch


# Global function
def gpx_to_heatmap(gpx_file, api_key, html_output, png_output, top_left_corner, bottom_right_corner):
    gpx_to_heatmap_gglmps(gpx_file, api_key, html_output)
    gpx_to_heatmap_pitch(gpx_file, png_output, top_left_corner, bottom_right_corner)



